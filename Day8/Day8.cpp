// Day8.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <algorithm>
#include <iostream>
#include <vector>

class Layer
{
private:
	static const int WIDTH = 25;
	static const int HEIGHT = 6;

	static const int BLACK = 0;
	static const int WHITE = 1;
	static const int TRANSPARENT = 2;

public:
	bool Load()
	{
		for (int i = 0; i < WIDTH*HEIGHT; i++)
		{
			char input;
			if (!std::cin.get(input))
				return false;
			pixels.push_back(input - '0');
		}
		return pixels.size() == WIDTH * HEIGHT;
	}

	void Clear()
	{
		pixels.clear();
	}

	void Merge(const Layer& layer)
	{
		if (pixels.empty())
		{
			pixels = layer.pixels;
			return;
		}

		std::transform(pixels.begin(), pixels.end(),
			layer.pixels.begin(), pixels.begin(),
			[](const int& firstLayer, const int& secondLayer)
		{
			if (firstLayer == TRANSPARENT)
				return secondLayer;
			return firstLayer;
		});

	}

	int Zeroes() const
	{
		if (pixels.empty())
			return INT_MAX;

		return std::count(pixels.begin(), pixels.end(), 0);
	}

	int Result() const
	{
		return std::count(pixels.begin(), pixels.end(), 1) *
			std::count(pixels.begin(), pixels.end(), 2);
	}

	void Print() const
	{
		for (int x = 0; x < HEIGHT; x++)
		{
			for (int y = 0; y < WIDTH; y++)
			{
				std::cout << pixels[y + x * WIDTH];
			}
			std::cout << std::endl;
		}
	}

private:
	std::vector<int> pixels;
};

int main()
{
	Layer result1, result2;
	for (Layer temp; temp.Load(); result2.Merge(temp), temp.Clear())
	{
		if (temp.Zeroes() < result1.Zeroes())
		{
			result1 = temp;
		}
	}
	std::cout << "PART 1 : " << result1.Result() << std::endl;
	std::cout << "PART 2 : " << std::endl;
	result2.Print();
}