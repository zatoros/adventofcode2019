#include <algorithm>
#include <functional>
#include <iterator>
#include <iostream>
#include <list>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <utility>

class Point : public std::pair<int, int>
{
public:
	Point(int a, int b, int s) : std::pair<int, int>(a, b), steps(s)
	{}

	int value() const
	{
		return std::abs(first) + std::abs(second);
	}

	int steps;
};

class WirePath : public std::set<Point>
{
public:
	void load()
	{
		using DirectionOperation = std::function<Point(Point&)>;
		static std::map<char, DirectionOperation> operations
		{
			{'L', [](Point& a) { ++a.steps; a.first -= 1; return a; } },
			{'R', [](Point& a) { ++a.steps; a.first += 1; return a; } },
			{'U', [](Point& a) { ++a.steps; a.second += 1; return a; }},
			{'D', [](Point& a) { ++a.steps; a.second -= 1; return a; }}
		};

		std::string buffer;
		std::getline(std::cin, buffer);

		std::istringstream inBuffer(buffer);
		char direction;
		int length;
		Point current(0, 0, 0);
		while (inBuffer >> direction)
		{
			inBuffer >> length;
			inBuffer.get();
			for (int i = 0; i < length; i++)
				insert(operations[direction](current));
		}
	}

private:
};

int main()
{
	WirePath wire1, wire2;
	wire1.load();
	wire2.load();

	int result1 = INT_MAX;
	int result2 = INT_MAX;
	for (const auto& point1 : wire1)
	{
		auto it = wire2.find(point1);
		if(it != wire2.end())
		{
			if (point1.value() < result1)
			{
				result1 = point1.value();
			}
			if (point1.steps + it->steps < result2)
			{
				result2 = point1.steps + it->steps;
			}
		}
	}

	std::cout << result1 << std::endl; // Task 1 result
	std::cout << result2 << std::endl; // Task 2 result
	return 0;
}