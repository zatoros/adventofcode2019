#include <iostream>
#include <list>

int main()
{
	const int MIN_PASSWORD = 137683;
	const int MAX_PASSWORD = 596253;

	int result1 = 0;
	int result2 = 0;
	for (int password = MIN_PASSWORD; password < MAX_PASSWORD; password++)
	{
		bool neverDecrease = true;
		bool adjacentsSame = false;
		
		std::pair<bool, int> adjacentsSame2(false, -1);
		int prevSameNumber = -1;

		std::list<int> digits;
		int currPassword = password;
		for (int digit = 0; digit < 6; digit++, 
			currPassword /= 10)
		{
			digits.push_front(currPassword % 10);
		}

		auto digitIt = digits.begin();
		int prevDigit = *digitIt;
		digitIt++;
		while(digitIt != digits.end())
		{
			int currDigit = *digitIt;
			digitIt++;
			if (prevDigit == currDigit)
			{
				adjacentsSame = true;
				if (!adjacentsSame2.first ||
					currDigit == adjacentsSame2.second)
				{
					adjacentsSame2.first = true;
					if (prevSameNumber != -1 &&
						prevSameNumber == currDigit)
					{
						adjacentsSame2.first = false;
					}
					adjacentsSame2.second = currDigit;
					prevSameNumber = prevDigit;
				}
			}
			else
			{
				prevSameNumber = -1;
			}
			if (prevDigit > currDigit)
			{
				neverDecrease = false;
				break;
			}
			prevDigit = currDigit;
		}
		if (neverDecrease && adjacentsSame)
		{
			result1++;
			if (adjacentsSame2.first)
			{
				result2++;
			}
		}
	}

	std::cout << result1 << std::endl; //Task 1 result
	std::cout << result2 << std::endl; //Task 2 result
	return 0;
}