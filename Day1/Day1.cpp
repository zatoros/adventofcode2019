#include <iostream>

int countFuelRequirement(int mass)
{
	int result = mass / 3 - 2;
	return result > 0 ? result : 0;;
}

int main()
{
	int mass;
	int fuelRequirement = 0;
	int fuelRequirementTotal = 0;
	while (std::cin >> mass)
	{
		int currFuelRequirement = countFuelRequirement(mass);

		fuelRequirement += currFuelRequirement;

		fuelRequirementTotal += currFuelRequirement;
		do
		{
			fuelRequirementTotal +=
				(currFuelRequirement = countFuelRequirement(currFuelRequirement));

		} while (currFuelRequirement > 0);
	}
	std::cout << fuelRequirement << std::endl; //Task 1 result
	std::cout << fuelRequirementTotal << std::endl; //Task 2 result
}