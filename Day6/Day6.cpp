#include <algorithm>
#include <iostream>
#include <set>
#include <string>
#include <utility>
#include <vector>

using Object = std::string;
using Objects = std::set<Object>;
using Orbit = std::pair<Object, Object>;
using Orbits = std::vector<Orbit>;


int main()
{
	Orbits orbits;
	Objects objects;

	std::string input;
	while (getline(std::cin, input))
	{
		Orbit orbit;
		orbit.first = input.substr(0, 3);
		orbit.second = input.substr(4, 3);
		orbits.push_back(orbit);
		objects.insert(orbit.first);
		objects.insert(orbit.second);
	}

	int results = 0;
	for (const auto& object : objects)
	{
		auto orbitIt = std::find_if(orbits.begin(), orbits.end(),
			[&object](const Orbit& orbit) {return orbit.second == object; });
		while (orbitIt != orbits.end())
		{
			results++;
			orbitIt = std::find_if(orbits.begin(), orbits.end(),
				[&orbitIt](const Orbit& orbit) {return orbit.second == orbitIt->first; });
		}
	}

	std::cout << results << std::endl; //Task 1 result

	Object youObject = std::find_if(orbits.begin(), orbits.end(),
		[](const Orbit& orbit) {return orbit.second == "YOU"; })->first;
	Object sanObject = std::find_if(orbits.begin(), orbits.end(),
		[](const Orbit& orbit) {return orbit.second == "SAN"; })->first;

	std::set<Orbit> path;
	for (const auto& object : { youObject, sanObject })
	{
		auto orbitIt = std::find_if(orbits.begin(), orbits.end(),
			[&object](const Orbit& orbit) {return orbit.second == object; });
		while (orbitIt != orbits.end())
		{
			if (path.count(*orbitIt) > 0)
				path.erase(*orbitIt);
			else
				path.insert(*orbitIt);
			orbitIt = std::find_if(orbits.begin(), orbits.end(),
				[&orbitIt](const Orbit& orbit) {return orbit.second == orbitIt->first; });
		}
	}

	std::cout << path.size() << std::endl; //Task 2 result
	return 0;
}