#include <algorithm>
#include <iostream>
#include <vector>
#include <queue>

class Program
{
public:
	enum Status
	{
		HOLD,
		HALT,
		PROCESS
	};


	void reset()
	{
		data = initial;
		position = 0;
		while (!input.empty())
			input.pop();
	}

	void reset(int noun, int verb)
	{
		reset();
		data[1] = noun;
		data[2] = verb;
	}

	int output() const
	{
		return data[0];
	}

	bool outVal(int & val)
	{
		if (out.empty())
			return false;

		val = out.front();
		out.pop();
		return true;
	}

	Status run()
	{
		Status status;
		while ((status = process()) == Status::PROCESS)
			next();
		return status;
	}

	std::vector<int>& memory() { return initial; }
	void setInput(int in) { input.push(in); }

private:
	static const int ADD_CODE = 1;
	static const int MULL_CODE = 2;
	static const int INPUT_CODE = 3;
	static const int OUTPUT_CODE = 4;
	static const int JUMP_TRUE_CODE = 5;
	static const int JUMP_FALSE_CODE = 6;
	static const int LESS_CODE = 7;
	static const int EQUALS_CODE = 8;
	static const int HALT_CODE = 99;

	static const int POSITION_MODE = 0;
	static const int IMMEDIATE_MODE = 1;

	std::vector<int> initial;
	std::vector<int> data;
	std::queue<int> input;
	std::queue<int> out;
	int position{ 0 };

	void next()
	{
		switch (code())
		{
		case ADD_CODE:
		case MULL_CODE:
		case LESS_CODE:
		case EQUALS_CODE:
			position += 4;
			break;
		case INPUT_CODE:
		case OUTPUT_CODE:
			position += 2;
			break;
		case JUMP_TRUE_CODE:
			if (arg(1))
				position = arg(2);
			else
				position += 3;
			break;
		case JUMP_FALSE_CODE:
			if (!arg(1))
				position = arg(2);
			else
				position += 3;
			break;
		}
	}

	int mode(int val)
	{
		return val % 10;
	}

	int& value(int mode, int pos)
	{
		if (POSITION_MODE == mode)
		{
			return data[data[pos]];
		}
		else /*if (IMMEDIATE_MODE == mode)*/
		{
			return data[pos];
		}
		return data[0]; //Should not get here
	}

	int& arg(int number)
	{
		return value(mode(data[position] / static_cast<int>(std::pow(10, number + 1))),
			position + number);
	}

	int code()
	{
		return data[position] % 100;
	}

	Status process()
	{
		switch (code())
		{
		case ADD_CODE:
			arg(3) =
				arg(1) + arg(2);
			return Status::PROCESS;
		case MULL_CODE:
			arg(3) =
				arg(1) * arg(2);
			return Status::PROCESS;
		case INPUT_CODE:
			if (!input.empty())
			{
				arg(1) = input.front();
				input.pop();
				return Status::PROCESS;
			}
			else
			{
				return Status::HOLD;
			}
		case OUTPUT_CODE:
			out.push(arg(1));
			return Status::PROCESS;
		case JUMP_TRUE_CODE:
		case JUMP_FALSE_CODE:
			return Status::PROCESS;
		case LESS_CODE:
			arg(3) = arg(1) < arg(2) ? 1 : 0;
			return Status::PROCESS;
		case EQUALS_CODE:
			arg(3) = arg(1) == arg(2) ? 1 : 0;
			return Status::PROCESS;
		case HALT_CODE:
			return Status::HALT;
		}
		return Status::HALT;
	}
};

int main()
{
	int number;
	Program program;
	while (std::cin >> number)
	{
		program.memory().push_back(number);
		std::cin.get();
	}

	int result1 = 0;
	std::vector<int> phaseSettings{ 0,1,2,3,4 };
	do
	{
		int currInput = 0;
		for (const int& phase : phaseSettings)
		{
			program.reset();
			program.setInput(phase);
			program.setInput(currInput);
			program.run();
			program.outVal(currInput);
		}
		if (currInput > result1)
		{
			result1 = currInput;
		}
	} while (std::next_permutation(phaseSettings.begin(), phaseSettings.end()));

	std::cout << result1 << std::endl; //Task 1 result

	int result2 = 0;
	std::vector<int> feedBackPhaseSettings{ 5,6,7,8,9 };
	program.reset();
	do
	{
		std::vector<Program> programs(5, program);
		for (unsigned index = 0; index < programs.size(); index++)
		{
			programs[index].setInput(feedBackPhaseSettings[index]);
		}
		programs[0].setInput(0);
		unsigned programIdex = 0;
		bool programHalt = false;
		do
		{
			Program& prevProgram = programs[programIdex];
			if (prevProgram.run() == Program::Status::HALT)
			{
				programHalt = true;
			}
			if (++programIdex == programs.size())
			{
				programIdex = 0;
			}
			Program& nextProgram = programs[programIdex];
			if (programHalt && programIdex == 0)
			{
				int currResult;
				prevProgram.outVal(currResult);
				if (currResult > result2)
					result2 = currResult;
				break;
			}
			int prevResult;
			while (prevProgram.outVal(prevResult))
			{
				nextProgram.setInput(prevResult);
			}
		} while (true);
		
	} while (std::next_permutation(
		feedBackPhaseSettings.begin(), feedBackPhaseSettings.end()));

	std::cout << result2 << std::endl; //Task 2 result

	return 0;
}