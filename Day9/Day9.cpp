#include <iostream>
#include <map>

class Program
{
public:
	using ArgsType = long long;
	using MemoryType = std::map<ArgsType, ArgsType>;

	void reset()
	{
		data = initial;
		position = 0;
		relativebase = 0;
	}

	void reset(ArgsType noun, ArgsType verb)
	{
		reset();
		data[1] = noun;
		data[2] = verb;
	}

	ArgsType output() const
	{
		return data.at(0ll);
	}

	void run()
	{
		while (process())
			next();
	}

	MemoryType& memory() { return initial; }
	void setInput(ArgsType in) { input = in; }

private:
	static const int ADD_CODE = 1;
	static const int MULL_CODE = 2;
	static const int INPUT_CODE = 3;
	static const int OUTPUT_CODE = 4;
	static const int JUMP_TRUE_CODE = 5;
	static const int JUMP_FALSE_CODE = 6;
	static const int LESS_CODE = 7;
	static const int EQUALS_CODE = 8;
	static const int ADJUST_RELATIVE_BASE = 9;
	static const int HALT_CODE = 99;

	static const int POSITION_MODE = 0;
	static const int IMMEDIATE_MODE = 1;
	static const int RELATIVE_MODE = 2;

	MemoryType initial;
	MemoryType data;
	ArgsType input;
	ArgsType position{ 0 };
	ArgsType relativebase{ 0 };

	void next()
	{
		switch (code())
		{
		case ADD_CODE:
		case MULL_CODE:
		case LESS_CODE:
		case EQUALS_CODE:
			position += 4;
			break;
		case INPUT_CODE:
		case OUTPUT_CODE:
		case ADJUST_RELATIVE_BASE:
			position += 2;
			break;
		case JUMP_TRUE_CODE:
			if (arg(1))
				position = arg(2);
			else
				position += 3;
			break;
		case JUMP_FALSE_CODE:
			if (!arg(1))
				position = arg(2);
			else
				position += 3;
			break;
		}
	}

	ArgsType mode(ArgsType val)
	{
		return val % 10;
	}

	ArgsType& value(ArgsType mode, ArgsType pos)
	{
		if (POSITION_MODE == mode)
		{
			return data[data[pos]];
		}
		else if (RELATIVE_MODE == mode)
		{
			return data[data[pos] + relativebase];
		}
		else /*if (IMMEDIATE_MODE == mode)*/
		{
			return data[pos];
		}
		return data[0]; //Should not get here
	}

	ArgsType& arg(ArgsType number)
	{
		return value(mode(data[position] / static_cast<ArgsType>(std::pow(10, number + 1))),
			position + number);
	}

	ArgsType code()
	{
		return data[position] % 100;
	}

	bool process()
	{
		switch (code())
		{
		case ADD_CODE:
			arg(3) =
				arg(1) + arg(2);
			return true;
		case MULL_CODE:
			arg(3) =
				arg(1) * arg(2);
			return true;
		case INPUT_CODE:
			arg(1) = input;
			return true;
		case OUTPUT_CODE:
			std::cout << arg(1) << std::endl;
			return true;
		case JUMP_TRUE_CODE:
		case JUMP_FALSE_CODE:
			return true;
		case LESS_CODE:
			arg(3) = arg(1) < arg(2) ? 1 : 0;
			return true;
		case EQUALS_CODE:
			arg(3) = arg(1) == arg(2) ? 1 : 0;
			return true;
		case ADJUST_RELATIVE_BASE:
			relativebase += arg(1);
			return true;
		case HALT_CODE:
			return false;
		}
		return false;
	}
};

int main()
{
	Program::ArgsType number, address = 0;
	Program program;
	while (std::cin >> number)
	{
		program.memory().emplace(address++, number);
		std::cin.get();
	}

	program.reset();
	program.setInput(1);
	program.run(); // Task 1 result will be printed

	program.reset();
	program.setInput(2);
	program.run(); // Task 2 result will be printed

	return 0;
}