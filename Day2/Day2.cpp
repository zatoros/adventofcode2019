#include <iostream>
#include <vector>

class Program
{
public:

	void reset(int noun, int verb)
	{
		data = initial;
		data[1] = noun;
		data[2] = verb;
		position = 0;
	}

	int output() const
	{
		return data[0];
	}

	void run()
	{
		while (process())
			next();
	}

	std::vector<int>& memory() { return initial; }

private:
	static const int ADD_CODE = 1;
	static const int MULL_CODE = 2;
	static const int HALT_CODE = 99;

	std::vector<int> initial;
	std::vector<int> data;
	int position{ 0 };

	void next()
	{
		position += 4;
	}

	int& result()
	{
		return data[data[position + 3]];
	}

	int arg1()
	{
		return data[data[position + 1]];
	}

	int arg2()
	{
		return data[data[position + 2]];
	}

	int code()
	{
		return data[position];
	}

	bool process()
	{
		switch (code())
		{
		case ADD_CODE:
			result() =
				arg1() + arg2();
			return true;
		case MULL_CODE:
			result() =
				arg1() * arg2();
			return true;
		case HALT_CODE:
			return false;
		}
		return false;
	}
};

int main()
{
	int number;
	Program program;
	while (std::cin >> number)
	{
		program.memory().push_back(number);
		std::cin.get();
	}

	program.reset(12, 2);
	program.run();
	std::cout << program.output() << std::endl; // Task 1 result 

	for (int noun = 0; noun <= 99; noun++)
	{
		for (int verb = 0; verb <= 99; verb++)
		{
			program.reset(noun, verb);
			program.run();
			if (program.output() == 19690720)
			{
				std::cout << noun * 100 + verb << std::endl; // Task 2 result
				return 0;
			}
		}
	}

	return 0;
}